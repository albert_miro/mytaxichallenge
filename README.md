# MyTaxiChallenge

MyTaxi Code Challenge

## Overview

I have used Kotlin programming language and a MVVM architecture to develop this project.

The project has a MainActivity that holds two fragments, the first one will show
the list of available taxis in the city of Hamburg, if the fleetType is TAXI you
will see a picture of a taxi cab, if there is a POOLING type, then the image is
a yellow sport car.

When the user press any of the list items, it will be redirected to the fragment that
shows the map along with all the taxi points. The map will zoom and show the selected
taxi in the center of the screen. For a better experience, is showed in the upper part
of the screen which is the current vehicle selected, showing the basic information of it 
(the same that is shown in the list item).

## Architecture

I have used an MVVM architecture because I have used this project also for learning, it's
a part of the Android Architecture Components and makes a good combination with LiveData
and RxJava.

What I liked most of this architecture, it is the reactive approach to communicate between the
view and the ViewModel.

## Libraries used

To develop this project I have used the more common libraries, like
Retrofit, RxJava2, Dagger2, Android Architecture components (LiveData, ViewModel),...

The classes in the package "di" where extracted from Google GitHub examples 
repositories and I have made some modifications to them.

## Tests

There are unit tests for the service part, to check that in different scenarios the data
will be parsed correctly and the app will work.

## Improvements

Some improvements that can be made are:
- Include more tests to cover more functions of the app
- Show a list of the current drivers in the city in the map, to switch quickly between them
- Improve the design :-) 
