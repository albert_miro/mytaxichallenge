package com.albertmiro.mytaxichallenge.domain

enum class FleetType {
    TAXI,
    POOLING,
    OTHER
}