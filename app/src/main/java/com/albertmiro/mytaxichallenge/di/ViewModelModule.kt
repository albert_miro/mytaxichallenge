package com.albertmiro.mytaxichallenge.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.albertmiro.mytaxichallenge.ui.viewmodel.AppViewModelFactory
import com.albertmiro.mytaxichallenge.ui.viewmodel.MyTaxiViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MyTaxiViewModel::class)
    abstract fun bindMyTaxiViewModel(myTaxiViewModel: MyTaxiViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}
