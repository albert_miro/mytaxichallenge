package com.albertmiro.mytaxichallenge.di

import com.albertmiro.mytaxichallenge.ui.taximap.TaxiMapFragment
import com.albertmiro.mytaxichallenge.ui.taxis.TaxiListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun contributeTaxiListFragment(): TaxiListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeTaxiMapFragment(): TaxiMapFragment

}
