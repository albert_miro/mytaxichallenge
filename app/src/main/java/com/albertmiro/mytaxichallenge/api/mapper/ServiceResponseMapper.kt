package com.albertmiro.mytaxichallenge.api.mapper

import com.albertmiro.mytaxichallenge.api.model.MyTaxiResponse
import com.albertmiro.mytaxichallenge.api.model.VehicleResponse
import com.albertmiro.mytaxichallenge.domain.FleetType
import com.albertmiro.mytaxichallenge.domain.Vehicle

/*
* Mapper methods to transform the service response to our model objects
*/

fun toVehicleList(response: MyTaxiResponse): List<Vehicle> {
    return response.poiList.map { toVehicle(it) }.toList()
}

fun toVehicle(vehicle: VehicleResponse): Vehicle {
    val fleetType: FleetType = when (vehicle.fleetType) {
        "TAXI" -> FleetType.TAXI
        "POOLING" -> FleetType.POOLING
        else -> FleetType.OTHER
    }

    return Vehicle(vehicle.id,
            Pair(vehicle.coordinate.latitude, vehicle.coordinate.longitude),
            fleetType,
            vehicle.heading)
}
