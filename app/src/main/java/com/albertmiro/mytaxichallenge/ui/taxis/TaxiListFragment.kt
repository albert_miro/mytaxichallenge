package com.albertmiro.mytaxichallenge.ui.taxis

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.albertmiro.mytaxichallenge.R
import com.albertmiro.mytaxichallenge.di.Injectable
import com.albertmiro.mytaxichallenge.domain.Vehicle
import com.albertmiro.mytaxichallenge.extensions.isVisible
import com.albertmiro.mytaxichallenge.extensions.showMessage
import com.albertmiro.mytaxichallenge.ui.common.BaseFragment
import com.albertmiro.mytaxichallenge.ui.loadTaxiMapFragment
import com.albertmiro.mytaxichallenge.ui.taxis.adapter.TaxiAdapter
import com.albertmiro.mytaxichallenge.ui.viewmodel.MyTaxiViewModel
import kotlinx.android.synthetic.main.fragment_taxi_list.*
import org.jetbrains.anko.support.v4.onRefresh
import javax.inject.Inject

class TaxiListFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var myTaxiViewModel: MyTaxiViewModel
    private lateinit var taxiAdapter: TaxiAdapter

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_taxi_list, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getViewModel()
        hideBackOnToolbar()
        initTaxiList()
        initSwipeRefresh()
        initObservers()

        myTaxiViewModel.loadTaxis(false)
    }

    private fun getViewModel() {
        myTaxiViewModel = ViewModelProviders.of(activity!!, viewModelFactory)
                .get(MyTaxiViewModel::class.java)
    }

    private fun hideBackOnToolbar() =
            mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(false)

    private fun initTaxiList() {
        taxiAdapter = TaxiAdapter().apply {
            onClickAction = {
                myTaxiViewModel.setCurrentTaxiId(it.id)
                mainActivity.loadTaxiMapFragment()
            }
        }

        val mLayoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false)

        taxiList.apply {
            adapter = taxiAdapter
            setHasFixedSize(true)
            layoutManager = mLayoutManager
        }
    }

    private fun initSwipeRefresh() {
        swipeRefresh.onRefresh {
            myTaxiViewModel.loadTaxis(true)
        }
    }

    private fun initObservers() {
        myTaxiViewModel.isDataLoading().observe(this, Observer {
            it?.let {
                if (!swipeRefresh.isRefreshing) {
                    progressBar.isVisible(it)
                }
            }
        })

        myTaxiViewModel.getTaxis().observe(this, Observer {
            it?.let {
                showTaxis(it)
            }
        })

        myTaxiViewModel.isNetworkError().observe(this, Observer {
            it?.let {
                if (it) {
                    mainActivity.showMessage(getString(R.string.lost_connection))
                    hideRefreshIcon()
                }
            }
        })

        myTaxiViewModel.isUnknownError().observe(this, Observer {
            it?.let {
                if (it) {
                    mainActivity.showMessage(getString(R.string.unexpected_error))
                    hideRefreshIcon()
                }
            }
        })
    }

    private fun showTaxis(taxis: List<Vehicle>) {
        if (swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }

        taxiAdapter.setItems(taxis)

        if (taxis.isEmpty()) {
            mainActivity.showMessage(getString(R.string.no_taxis))
        }
    }

    private fun hideRefreshIcon() {
        if (swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }
    }
}