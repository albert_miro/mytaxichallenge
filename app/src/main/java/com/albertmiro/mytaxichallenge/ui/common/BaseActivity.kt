package com.albertmiro.mytaxichallenge.ui.common

import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()
