package com.albertmiro.mytaxichallenge.ui.common

import android.support.v4.app.Fragment
import com.albertmiro.mytaxichallenge.ui.MainActivity

open class BaseFragment : Fragment() {
    val mainActivity: MainActivity
        get() = activity as MainActivity
}
