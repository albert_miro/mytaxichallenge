package com.albertmiro.mytaxichallenge.ui

import com.albertmiro.mytaxichallenge.R
import com.albertmiro.mytaxichallenge.ui.taximap.TaxiMapFragment
import com.albertmiro.mytaxichallenge.ui.taxis.TaxiListFragment

fun MainActivity.loadTaxiListFragment() {
    supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, TaxiListFragment())
            .commit()
}

fun MainActivity.loadTaxiMapFragment() {
    val taxiMapFragment = TaxiMapFragment()
    supportFragmentManager.beginTransaction()
            .addToBackStack(taxiMapFragment.javaClass.name)
            .replace(R.id.fragmentContainer, taxiMapFragment)
            .commit()
}
