package com.albertmiro.mytaxichallenge.repository

import com.albertmiro.mytaxichallenge.domain.Vehicle
import io.reactivex.Single

interface TaxiVehiclesRepository {
    fun getHamburgTaxis(forceRefresh: Boolean): Single<List<Vehicle>>
}