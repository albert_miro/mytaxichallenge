package com.albertmiro.mytaxichallenge.repository

const val HAMBURG_LATITUDE_1 = 53.694865
const val HAMBURG_LONGITUDE_1 = 9.757589
const val HAMBURG_LATITUDE_2 = 53.394655
const val HAMBURG_LONGITUDE_2 = 10.099891